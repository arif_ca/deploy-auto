#!/bin/bash
# Deploy files using SFTP http://manpages.ubuntu.com/manpages/trusty/en/man1/sftp.1.html
# Bitbucket Pipelines
#
# Required globals:
#   SERVER
#   USER
#   REMOTE_PATH
#   PASSWORD
#
# Optional globals:
#   LOCAL_PATH
#   DEBUG
#   EXTRA_ARGS
#   DELETE_FLAG

source "$(dirname "$0")/common.sh"

LFTP_DEBUG_ARGS=
## Enable debug mode.
enable_debug() {
  if [[ "${DEBUG}" == "true" ]]; then
    info "Enabling debug mode."
    set -x
    LFTP_DEBUG_ARGS="-vvv"
  fi
}

validate() {
  # mandatory parameters
  : USER=${USER:?'USER variable missing.'}
  : PASSWORD=${PASSWORD:?'PASSWORD variable missing'}
  : SERVER=${SERVER:?'SERVER variable missing'}
  : REMOTE_PATH=${REMOTE_PATH:?'REMOTE_PATH variable missing.'}
  : LOCAL_PATH=${LOCAL_PATH:="${BITBUCKET_CLONE_DIR}"}
  : DELETE_FLAG=${DELETE_FLAG:="true"}

  ARGS_STRING=""

  if [ -n "${DELETE_FLAG}" ]; then
    if [ "${DELETE_FLAG}" == "true" ]; then
      ARGS_STRING="${ARGS_STRING} --delete-first"
    elif [ "${DELETE_FLAG}" != "false" ]; then
      fail "Value of the DELETE_FLAG has to be either true or false."
    fi
  fi
}

run_pipe() {
    info "Starting FTP deployment to ${SERVER}:${REMOTE_PATH}..."
    set +e
    lftp -u $USER,$PASSWORD -e "set ftp:ssl-allow no; mirror ${ARGS_STRING} ${LFTP_DEBUG_ARGS} ${EXTRA_ARGS} -R ${LOCAL_PATH} ${REMOTE_PATH};quit" $SERVER
    set -e

    STATUS=$?

    if [[ "${STATUS}" == "0" ]]; then
      success "Deployment finished."
    else
      fail "Deployment failed."
    fi

    exit $STATUS
}

validate
enable_debug
run_pipe
