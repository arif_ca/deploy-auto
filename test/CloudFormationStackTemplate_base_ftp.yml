AWSTemplateFormatVersion: 2010-09-09

Description: >-
  AWS CloudFormation Template EC2InstanceWithSecurityGroupSample: Create
  an Amazon EC2 instance running the Amazon Linux AMI. The AMI is chosen based
  on the region in which the stack is run.
Parameters:
  KeyName:
    Description: Name of an existing EC2 KeyPair to enable SSH access to the instance
    Type: 'AWS::EC2::KeyPair::KeyName'
    ConstraintDescription: must be the name of an existing EC2 KeyPair.
  SSHLocation:
    Description: The IP address range that can be used to SSH to the EC2 instances
    Type: String
    MinLength: '9'
    MaxLength: '18'
    Default: 0.0.0.0/0
    AllowedPattern: '(\d{1,3})\.(\d{1,3})\.(\d{1,3})\.(\d{1,3})/(\d{1,2})'
    ConstraintDescription: must be a valid IP CIDR range of the form x.x.x.x/x.
  PASSWORD:
    NoEcho: true
    Type: String
    Description: Password for EC2 instance for ftp password test
Resources:
  EC2Instance:
    Type: 'AWS::EC2::Instance'
    Properties:
      InstanceType: t2.micro
      ImageId: ami-00eb20669e0990cb4
      SecurityGroups:
        - !Ref InstanceSecurityGroup
      KeyName: !Ref KeyName
      Tags:
        - Key: owner
          Value: akyrdan
        - Key: team
          Value: bitbucket-pipelines
        - Key: Name
          Value: akyrdan-ftpdeploy-instance
      UserData:
        Fn::Base64: !Sub |
              #!/bin/bash
              yum update
              yum install nginx vsftpd -y
              /etc/init.d/nginx start
              /etc/init.d/vsftpd start

              cd /usr/share/nginx/html
              mkdir -p ftp
              chown ec2-user:ec2-user ftp
              echo 'hello' > ftp/hello.txt
              chown ec2-user:ec2-user ftp/hello.txt
              /etc/init.d/nginx restart

              sed -i "s/PasswordAuthentication no/PasswordAuthentication yes/g" /etc/ssh/sshd_config
              echo "" >> /etc/ssh/sshd_config
              echo "Port 22" >> /etc/ssh/sshd_config
              /etc/init.d/sshd restart

              sed -i "s/anonymous_enable=YES/anonymous_enable=NO/g" /etc/vsftpd/vsftpd.conf
              sed -i "s/listen_ipv6=YES/listen_ipv6=NO/g" /etc/vsftpd/vsftpd.conf
              sed -i "s/listen=NO/listen_ipv6=YES/g" /etc/vsftpd/vsftpd.conf
              echo "" >> /etc/vsftpd/vsftpd.conf
              echo "pasv_enable=YES" >> /etc/vsftpd/vsftpd.conf
              echo "pasv_min_port=1024" >> /etc/vsftpd/vsftpd.conf
              echo "pasv_max_port=1048" >> /etc/vsftpd/vsftpd.conf
              echo "pasv_address=0.0.0.0" >> /etc/vsftpd/vsftpd.conf
              echo "local_root=/usr/share/nginx/html/ftp" >> /etc/vsftpd/vsftpd.conf
              chkconfig --level 345 vsftpd on
              /etc/init.d/vsftpd restart

              echo -e "${PASSWORD}\n${PASSWORD}" | passwd ec2-user
              gpasswd -a ec2-user ftp
              /etc/init.d/sshd restart
              /etc/init.d/vsftpd restart
  InstanceSecurityGroup:
    Type: 'AWS::EC2::SecurityGroup'
    Properties:
      GroupDescription: Enable access
      SecurityGroupIngress:
        - IpProtocol: tcp
          FromPort: '22'
          ToPort: '22'
          CidrIp: !Ref SSHLocation
        - IpProtocol: tcp
          FromPort: '80'
          ToPort: '80'
          CidrIp: !Ref SSHLocation
        - IpProtocol: tcp
          FromPort: '20'
          ToPort: '21'
          CidrIp: !Ref SSHLocation
        - IpProtocol: tcp
          FromPort: '1024'
          ToPort: '1048'
          CidrIp: !Ref SSHLocation
Outputs:
  InstanceId:
    Description: InstanceId of the newly created EC2 instance
    Value: !Ref EC2Instance
  AZ:
    Description: Availability Zone of the newly created EC2 instance
    Value: !GetAtt 
      - EC2Instance
      - AvailabilityZone
  PublicDNS:
    Description: Public DNSName of the newly created EC2 instance
    Value: !GetAtt 
      - EC2Instance
      - PublicDnsName
  PublicIP:
    Description: Public IP address of the newly created EC2 instance
    Value: !GetAtt 
      - EC2Instance
      - PublicIp
